# GitLab License management changelog

## 11-6-stable

## 11-5-stable

## 11-4-stable

- Allow `SETUP_CMD` to skip auto-detection of build tool

## 11-3-stable

## 11-2-stable

## 11-1-stable

## 11-0-stable
- Initial release
